const mongoose = require('mongoose')

const dbHost = process.env.DB_HOST || "localhost";
const dbPort = process.env.DB_PORT || 27017;
const dbName = process.env.DB_NAME || "my_db_name";
const mongoUrl = `mongodb://${dbHost}:${dbPort}/${dbName}`;


const connectDb = async () => {
    try {
        const conn = await mongoose.connect(mongoUrl)
        if (conn.connection.readyState === 1) console.log('Database connected')
        else console.log('Database connecting...')
    } catch (error) {
        console.log("Database connection failed, try reconnecting...")
				setTimeout(connectDb, 5000)
    }
}

module.exports = connectDb
